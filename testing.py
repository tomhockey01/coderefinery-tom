def add(a,b):
    return a + b

def test_add():
    #-- the test will have the outcome 'fail' if one of the asserts raises an error

    assert add(2, 3) == 5
    assert add('space', 'ship') == 'spaceship'
    
    #-- this produces an error
    # assert add(5, 'age') > 12

def substract(a, b):
    return a - b # <---- fix this in step 8

# uncomment the following test in step 5
def test_substract():
   assert substract(2, 3) == -1